const fs 						  = require('fs-extra');
const express 		    = require('express')
const router          = express.Router();
const path 			      = require('path');
const app 		      	= express()
const csrf 			      = require('csurf')
const nunjucks 		    = require('nunjucks');
const apiClass		      = require('./routes/api.js');
const publicAccess      = require('./routes/publicAccess.js');
const cookieParser 	    = require('cookie-parser');
const bodyParser 		    = require('body-parser');
const session 		      = require('express-session');
const MongoDBStore 	    = require('connect-mongodb-session')(session);
const passport          = require('passport');
const User              = require('./model/user');
const {mongoose,uriString} = require('./classes/mongooseClass');
// session express js
const store = new MongoDBStore(
  {
    uri: uriString,
    collection: 'mySessions'
});
// Passport does not directly manage your session, it only uses the session.
// So you configure session attributes (e.g. life of your session) via express
const sessionOpts = {
  saveUninitialized: false, // saved new sessions
  resave: false, // do not automatically write to the session store
  store: store,
  secret: 'avmadkfvk',
  cookie: { 
    secure: false,
    httpOnly: true, 
    maxAge: 2419200000 } // configure when sessions expires
}
nunjucks.configure([path.join(__dirname, 'views/'),path.join(__dirname, 'views/pages/')], {
  autoescape: true,
  express: app,
  watch:true,
  noCache:true
});
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(cookieParser());
app.use(csrf({ cookie: true }))
app.use(session(sessionOpts));
app.use(passport.initialize());
app.use(passport.session());
app.set('view engine','html');
// define static public folder
app.use('/static', express.static(path.join(__dirname,'./','public')))
app.use(function(req,res,next){
  // res.locals.base_url = req.protocol+'//'+req.get('host');
  res.locals.csrfToken = req.csrfToken();
  res.locals.user_id = req.session.user_id;
  if(req.session.user_id){
    res.locals.is_login = true;
    var gg = new (User)();
    gg.getCurrentUser({
      _id : req.session.user_id
    },function(data){
      res.locals.current_data = data;
      return next();
    },function(error){
      if(error) return res.status(500).send({
        status : 'error',
        message : error.message
      })
      return next();
    })
  }else{
    res.locals.is_login = false;
    next();
  }
})

app.use('/api',apiClass(router));
app.use('/',publicAccess(router));
if(!fs.existsSync(path.join(__dirname,'./','storages'))){
  fs.mkdirSync(path.join(__dirname,'./','storages'));
  fs.mkdirSync(path.join(__dirname,'./','storages/temps'));
  fs.mkdirSync(path.join(__dirname,'./','storages/defaults'));
  fs.mkdirSync(path.join(__dirname,'./','storages/uploads'));
}
// Handle 404
// app.use(function(req, res, next) {
//   res.status(404).send('aaaaaaaaaaaaaaaa');
// });
  
// // Handle 500
// app.use(function(error, req, res, next) {
//   res.send('500: Internal Server Error', 500);
// });

// Handle 404
// app.use(function(req, res) {
//     res.status(404);
//     res.render('404.html', {title: '404: File Not Found'});
// });

// Handle 500
// app.use(function(error, req, res, next) {
//     res.status(500);
//     res.render('500.html', {title:'500: Internal Server Error', error: error});
// });
// console.log(process.env.PORT);
app.listen(8082, process.env.IP || "0.0.0.0", function(){
  // var addr = app.address();
   console.log("App server listening");
});
