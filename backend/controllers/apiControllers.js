const fs 						= require('fs-extra');
const path 			      		= require('path');
const multiparty 				= require('multiparty');
const runnerImageStorage		= require('../classes/runnerImageStorage.js');
const bcrypt 					= require('bcrypt');
const saltRounds 				= 10;
const Validator 				= require('validatorjs');
const local_user 				= require('../passportjs/localUser.js');
const _ 						= require('lodash');
const {weetQl,getMiddleware}	= require('../classes/apiFilter.js');
let init = {};
init.staticFile = function(req,res,next){
	if(path.extname(req.query.filename).substring(1) != ''){
		let oo = new (runnerImageStorage)();
		oo.setDefinePathFolder(path.join(__dirname,'../','storages/defaults'));
		oo.setFileType(path.extname(req.query.filename).substring(1));
		oo.getImage(req.query.filename,req.query.mode,function(data){
			res.writeHead(200, {'Content-Type': 'image/jpeg'});
	    	res.end(data); // Send the file data to the browser.
		},function(err){
			res.status(500).send({
				status : 'error',
				message : err.message
			});
		})
	}else{
		res.status(500).send({
			status : 'error',
			message : 'Tolong Pake Extentionnya'
		});
	}
}
init.assetFile = function(req,res,next){
	let oo = new (runnerImageStorage)();
	oo.setDefinePathFolder(path.join(__dirname,'../','storages/uploads'));
	oo.setFileType(path.extname(req.query.filename).substring(1));
	oo.getImage(req.query.filename,req.query.mode,function(data){
		res.writeHead(200, {'Content-Type': 'image/jpeg'});
    	res.end(data); // Send the file data to the browser.
	},function(err){
		res.status(500).send({
			status : 'error',
			message : err.message
		});
	})
}
init.uploadFile = async function(req,res,next){
	var form = new multiparty.Form({
		autoFiles : true,
		maxFilesSize: 1048576,
		uploadDir : path.join(__dirname,'../','storages/temps')
	});
	form.parse(req, function(err, fields, files) {
		if (err) return res.status(500).send({
					status : 'error',
					message : err.message
				});
    	let oo = new (runnerImageStorage)();
		oo.setImagePacking(path.join(__dirname,'../','storages/uploads'),files.imageUpload[0],function(){
			oo.save(function(){
				// siapkan table untuk simpan nama file nya disini
				switch(fields.action[0]){
					case 'image-profile':
						let usernya = require('../model/user');
						usernya = new (usernya)();
						usernya.saveImageProfile({
							_id : req.session.user_id
						},{
							image_profile : oo.getImageName()
						},function(data){
							res.send({
								status : 'success',
								message : data
							});
						},function(err){
							res.status(500).send({
								status : 'error',
								message : err.message
							});
						})
						
					break;
					case 'image-header':
					break;
				}
			},function(err){
				res.status(500).send({
					status : 'error',
					message : err.message
				});
			});
		});
	});
}
let aa = 0;
init.simpleApi = function (req, res, next) {
	console.log('res =>',aa+=1);
  	weetQl(res,req.body,init.apiAction(req,res,next));
}
init.apiAction = function(req, res, next){
	let vm = this;
	// define model remember case sensitive
	vm.UserMember = require('../model/user');
	vm.testDriveFunction = function(q,obj){
		let result = getRequireFilter(obj,['testDriveChildren']);
	    if(result.rejected()){
	        return res.status(500).send({
	        	message : result.message()
	        });
	    }
	}
	vm.signinMemberProcess = function(r){
		local_user.signIn(function(username,password,done){
			process.nextTick(function () {
				let setObjectUserLogin = {
					username : username,
					password : password
				}
				let setLogin = {};
				let validation = new Validator(setObjectUserLogin, {
					username: 'required|email'
				});
				if(validation.fails()){
					setLogin.username = username;
				}else{
					setLogin.email = username;
				}
				let userEmail = new (vm.UserMember)();
				validation = new Validator(setObjectUserLogin, {
					password: 'required|min:6'
				}); 
				if(validation.fails()){
					return res.status(500).send({
						status : 'error',
						message : validation.errors
					})
				}
				userEmail.findOne(setLogin).exec(function(err,data){
					let gg = data;
					if(err) return res.status(500).send(err.message);
					if(data == null) return res.status(500).send({
							status : 'rejected',
							message : 'Wrong Username or password'
						})
					if(bcrypt.compareSync(setObjectUserLogin.password, data.password)){
						let oo = _.omit(JSON.parse(JSON.stringify(gg)),"password")
						// return done(null,oo);
						console.log(oo);
						req.session.user_id = oo._id;
						req.session.save(function(){
							res.header('Access-Control-Allow-Credentials', 'true');
							var fullUrl = req.protocol + '://' + req.get('host') + '/'+data.username;
							return res.status(200).send({
								status : 'success',
								message : 'Login Succesfully!',
								foward : fullUrl
							})
						});
            			
					}else{
						return res.status(500).send({
							status : 'rejected',
							message : 'Wrong Username or password'
						})
					}
				});
			});
		})
		req.body.username = r.username;
		req.body.password = r.password;
		local_user.passport.authenticate('local', {
	    // kalo pake ajax ini enggak ngaruh di ajax
	    // successRedirect: '/login/register',
	    // failureRedirect: '/login',
	    // failureFlash: true
  		})(req, res, next)
	}
	vm.newRegisterMember = async function(r){
		let result = await getMiddleware(r,[vm.checkEmailMemberExistMiddleware,vm.checkMemberUsernameExistMiddleware],function(err){
			// missing functtion
			if(err==true) return res.status(500).send({
				status : 'error',
				message : 'Any function is missing!'
			})
			// from middleware
			return res.status(500).send(err);
		});
		var validation = new Validator(r, {
			username: 'required|min:4',
		    password: 'required|min:6',
		    email: 'required|email',
		    full_name: 'required'
		});
		if(validation.fails()){
			return res.status(500).send({
				status : 'error',
				message : validation.errors
			})
		}
		bcrypt.genSalt(saltRounds, function(err, salt) {
			bcrypt.hash(r.password, salt, function(erCript, hash) {
				if(erCript) return res.status(500).send({
					status : 'error',
					message : erCript.message
				})
			  	// Store hash in your password DB.
			  	r.password = hash;
			  	r.status = 1;
			  	r.is_banned = false;
			  	let usernya = new (vm.UserMember)();
			  	usernya.create(r,function(err,data){
					if(err) return res.status(500).send({
						status : 'error',
						message : err.message
					})
					var fullUrl = req.protocol + '://' + req.get('host') + '/signin';
					res.status(200).send({
						status : 'success',
						message : 'success register',
						foward : fullUrl
					})
					
				})
			});
		})
	}
	
	// fetch
	vm.fetchAdmins = function(q,obj){
		return q;
	}
	// middleware
	vm.checkEmailMemberExistMiddleware = function(result,opts,next,error){
		let userEmail = new (vm.UserMember)();
		userEmail.findOne()
			.where('email').equals(opts.email)
			.exec(function(err,data){
				let messageError = {
					status : 'error',
					message : 'Email allready exist'
				}
				if(data != null) {
					return error(messageError);
				}
				next(true);
			})
	}
	vm.checkMemberUsernameExistMiddleware = function(result,opts,next,error){
		let userEmail = new (vm.UserMember)();
		userEmail.findOne()
			.where('username').equals(opts.username)
			.exec(function(err,data){
				let messageError = {
					status : 'error',
					message : 'Username allready exist'
				}
				if(data != null) {
					return error(messageError);
				}
				next(true);
			})
	}
	return vm;
}
module.exports = {
	...init
}