var randomstring 		= require("randomstring");
var uniqid 				= require('uniqid');
var helperStorageImage 	= require('./helperImageStorage.js');
module.exports = function(){
	let vm = this;
	vm.file_type = 'png';
	vm.save_folder = '';
	vm.the_files = '';
	vm.the_file_name = '';
	vm.option_file_name = [];
	vm.setDefinePathFolder = function(whatFolder){
		vm.save_folder = whatFolder;
	}
	vm.setImagePacking = function(whatFolder,theFiles,next){
		vm.save_folder = whatFolder;
		vm.the_files = theFiles;
		let fileName = '';
		hash = randomstring.generate({
			length: 30,
			charset: 'hex'
		});
		vm.the_file_name = fileName = hash+(new Date().toISOString().slice(0,10))+(new Date().getTime());
		vm.option_file_name['original'] = fileName+'.'+vm.file_type;
		vm.option_file_name['thumbnail'] = fileName+'_thumbnail.'+vm.file_type;
		vm.option_file_name['icon'] = fileName+'_icon.'+vm.file_type;
		next();
	}
	// define manual
	vm.setFileType = function(oo){
		vm.file_type = oo;
	}
	// informasi ini nanti di simpan di table terkait
	vm.getImageName = function(){
		return vm.option_file_name['original'];
	}
	vm.getImageNameWithPath = function(){
		return vm.save_folder+'/'+vm.option_file_name['original'];
	}
	vm.getImageSize = function(){
		return vm.file_type;
	}
	vm.delete = function(theName){
		// let imageStorage = 

	}
	vm.save = function(success,error){
		let oo = new (helperStorageImage)(vm.save_folder);
		oo.setSaveImage(vm.the_files,async function(){
			await oo.save(vm.option_file_name['original'],null,null,function(err){
				error(err);
			});
			await oo.save(vm.option_file_name['thumbnail'],120,null,function(err){
				error(err);
			});
			await oo.save(vm.option_file_name['icon'],60,null,function(err){
				error(err);
			});
			success();
		})
	}
	vm.getImage = async function(theName,mode,success,error){
		let whatName = theName.substr(0,theName.indexOf('.'));
		switch (mode) {
			case 'thumbnail':
				whatName = whatName+'_thumbnail.'+vm.file_type;
				break;
			case 'icon':
				whatName = whatName+'_icon.'+vm.file_type;
				break;
			default:
				// original
				whatName = theName;
				break;
		}
		
		let oo = new (helperStorageImage)();
		let data = await oo.fetchImage(vm.save_folder+'/'+whatName,function(err){
			if (err) return error(err);
		})
		success(data);
		return;
	}
	return vm;
}