const bodyParser = require('body-parser');
const {
	simpleApi,
	staticFile,
	assetFile,
	uploadFile,
} = require('../controllers/apiControllers.js');
let initRoute = function(router){
	router.get('/default',staticFile);
	router.get('/file',assetFile);
	router.post('/upload',bodyParser.urlencoded({ extended: false }),uploadFile);
	router.post('/post', simpleApi);
	return router;
}
module.exports = function(router){
	return initRoute(router);
};