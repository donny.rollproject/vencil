var User			= require('../model/user.js');
var checkAuthUser = function(req,res,next){
	if(req.session.user_id){
		next();
	}else{
		res.redirect('/signin');
	}
}
var hasAuth = function(req,res,next){
	if(req.session.user_id){
		res.redirect('/');
	}else{
		next();	
	}
}
let initRoute = function(router){
	router.get('/example-script',function(req,res,next){
		res.render('example-script',{
			title : 'Script Example'
		})
	})
	router.get('/signout',function(req,res,next){
		req.session.destroy(function(err){  
	        if(err){  
	            console.log(err);  
	        }  
	        res.redirect('/signin'); 
	    });  
	})
	router.get('/signin',hasAuth,function(req,res,next){
		res.render('user-signin',{
			title : 'Sign In | Vencil',
		})
	})
	router.get('/signup',hasAuth,function(req,res,next){
		res.render('user-signup',{
			title : 'Register | Vencil',
		})
	})
	router.get('/settings',function(req,res,next){
		res.render('user-settings',{
			title : 'Settings | Vencil'
	    })
	})
	router.get('/:username',checkAuthUser,function(req,res,next){
		let userMember = new (User)();
		userMember.findOne({
			username : req.params.username
		}).exec(function(err,data){
			if(data != null){
				res.render('user-profile',{
					title : data.full_name +' | Vencil',
					data : data,
				});
			}else{
				next(err);
			}
		})
	})
	router.get('/',function(req,res,next){
		
		res.render('home',{
			title : 'Vencil'
	    })
	    
		//console.log('locals => ',res);
	})
	return router;
}
module.exports = function(router){
	return initRoute(router);
}