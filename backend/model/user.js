const {mongoose} = require('../classes/mongooseClass');
const counter = require('./counter.js');
const Schema = mongoose.Schema;
let userSchema = new Schema({
  _no : {
    type: Number,
    index: true,
    unique: true
  },
  username:  {
    type: String,
    index: true,
    unique: true
  },
  password: String,
  full_name: String,
  email: {
    type: String,
    unique: true
  },
  date: { type: Date, default: Date.now },
  status: Boolean,
  is_banned: Boolean,
  image_profile:{
    type: String,
    default:null
  }
},
  {timestamps: { createdAt: 'created_at', updatedAt : 'updated_at' }}
);
// predefine auto increment
userSchema.pre('save', function(next) {
    var doc = this;
    if(doc._no == null){
      counter.findByIdAndUpdate({_id: 'user'}, {$inc: { seq: 1} },{
        // new: true,
        upsert: true
      }).then(function(count) {
          console.log("...count: "+JSON.stringify(count));
          doc._no = count.seq;
          next();
      }).catch(function(error) {
          console.error("counter error-> : "+error);
          throw error;
      });
    }else{
      next();
    }
});
let UserMember = mongoose.model('user',userSchema);

UserMember.limit = {
      'max_row' : 100,
      'row' : 10,
      'column' : [
          'column1','column2','column3'
      ]
  };

UserMember.saveImageProfile = function(opts,newData,success,error){
  let gg = UserMember.findOne(opts).exec(function(err,data){
    if(err) return error(err);
    if(data != null){
      data.image_profile = newData.image_profile;
      data.save(function(err,data){
        if(err) return error(err);
        success(data);
      })
    }else{
      success(null)
    }
  })
}
UserMember.getCurrentUser = function(opts,success,error){
  let dd = new Promise(function(resolve,rejected){
    let gg = UserMember.findOne(opts).exec(function(err,data){
      if(err) return rejected(err);
      if(data != null){
        resolve(data);
      }else{
        resolve(null)
      }
    })
  })
  dd.then(function(result){
    success(result);
  },function(err){
    error(err);
  })
}
UserMember.getAll = function(opts,success,error){
  let dd = new Promise(function(resolve,rejected){
    let gg = UserMember.find(opts).exec(function(err,data){
      if(err) return rejected(err);
      if(data != null){
        resolve(data);
      }else{
        resolve([])
      }
    })
  })
  dd.then(function(result){
    success(result);
  },function(err){
    error(err);
  })
}
UserMember.getUserProfile = UserMember.getCurrentUser;
module.exports = function(){
    return UserMember
};

