// **
// tips tips path folder
// [path] adalah folder path project itu sendiri jadi pas compile folder project juga ikut jadi sama
// 
// **

const path              = require('path');
const webpack           = require('webpack');
const htmlPlugin        = require('html-webpack-plugin');
const dashboardPlugin   = require('webpack-dashboard/plugin');
const autoprefixer      = require('autoprefixer'); 
const configuration     = new (require('./frontdev/configuration.js'))();
var CopyWebpackPlugin   = require('copy-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractSass       = new ExtractTextPlugin('assets/css/style.css');
const PATHS = {
  app: path.join(__dirname,'frontdev/src'),
  assets:path.join(__dirname,'frontdev/src/assets'),
  build: path.join(__dirname,'backend/public/','dist')
};
module.exports = {
  entry: {
    app: [PATHS.app,PATHS.assets]
  },
  output: {
    path: PATHS.build,
    // filename: 'bundle.[hash].js'
    filename: configuration.mode_option.debug==true?'bundle.js':'bundle.[hash].js',
    chunkFilename:configuration.mode_option.debug==true?'[name].chunk.js':'[name].[hash].chunk.js',
    publicPath: configuration.mode_option.debug == true ? configuration.path_source.DEBUG_URL : configuration.path_source.DIST_URL
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(bower_components)/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: false,
          presets: []
        }
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            // attrs: [':data-src']
          }
        }
      },
      {
        test: /\.(tag)?$/,
        loader: "tag-loader",
        exclude: /(node_modules|bower_components)/,
      },
      {
        test: /\.(css|less)?$/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }]
      },
      {
        test: /\.(scss|sass)?$/,
        use: extractSass.extract(['css-loader','sass-loader'])
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
       {
        test: /\.(eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,        
        loader: 'file-loader',
        query: {
          // [path] adalah folder path project itu sendiri jadi pas compile folder project juga ikut jadi sama
          // test aja
          name: 'assets/fonts/[name].[ext]',
        }
      },
        {
        test: /\.(ico|jpg|png|gif)(\?.*)?$/,        
        loader: 'file-loader',
        query: {
          // [path] adalah folder path project itu sendiri jadi pas compile folder project juga ikut jadi sama
          // test aja
          name: 'assets/images/[name].[ext]',
        }
      },   
    ],
  },
  plugins:[
    extractSass,
    new dashboardPlugin(),
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
    }),
    new CopyWebpackPlugin([
        { from: './frontdev/src/assets/fonts', to: 'assets/fonts' },
        { from: './frontdev/src/assets/images', to: 'assets/images' }
      ],{
          // By default, we only copy modified files during
          // a watch or webpack-dev-server build. Setting this
          // to `true` copies all files.
          copyUnmodified: true,
          force:true
      })
  ]
};