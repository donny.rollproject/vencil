const localStaticVariable = {
	url_root:'http://localhost:3000',
	url_restapi:'http://localhost:3000',
	url_storage:'http://localhost:3000/images'
}

const lanStaticVariable = {
    url_root:'http://localhost/the_jobs/html/laravel/public',
	url_restapi:'http://localhost/the_jobs/html/laravel/public',
	url_storage:'http://localhost/the_jobs/html/laravel/public/images'
}

const onlineStaticVariable = {
	url_root:'https://sociate.co',
	url_restapi:'https://sociate.co',
	url_storage:'https://sociate.co/images'
}

const cloud9Variable = {
	url_root:'http://vencil-rolldone.c9users.io',
	url_restapi:'http://vencil-rolldone.c9users.io',
	url_storage:'http://vencil-rolldone.c9users.io'
}

const cloud9Variable2 = {
	url_root:'http://vencil-rolldone.c9users.io:8082',
	url_restapi:'http://vencil-rolldone.c9users.io:8082',
	url_storage:'http://vencil-rolldone.c9users.io:8082'
}


export const staticVariable = cloud9Variable2;