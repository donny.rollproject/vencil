define(function(){
    let cropit = require('cropit');
    let _cropit = function(callback){
        this.setOnSaving = false;
        this.object = null;
        this.init = function(idELement,value){
            if(idELement == null)
                return;
            value.onImageError = function(error,code,message){
                var errorObject = {
                    error : error,
                    code : code,
                    message : message
                }
                vm.onEvent('onImageError',errorObject);
            }
            value.onImageLoaded = function(){
                vm.onEvent('onImageLoaded',null);
            }
            value.onImageLoading = function(){
                vm.onEvent('onImageLoading',null);
            }
            value.onFileReaderError = function(){
                vm.onEvent('onFileReaderError',null);
            }
            value.onFileChange = function(file){
                vm.onEvent('onEventListening',file);
            }
            value.onZoomEnabled = function(){
                vm.onEvent('onZoomChange',null);
            }
            value.onZoomDisabled = function(){
                vm.onEvent('onZoomDisabled',null);
            }
            value.onZoomChange = function(zoom){
                vm.onEvent('onZoomChange',zoom);
            }
            value.onOffsetChange = function(offset){
                vm.onEvent('onOffsetChange',offset);
            }
            vm.object = $(idELement).cropit(value);
            vm.disable();

        }
        this.onEvent = callback;
        this.setImageSrc = function(val){
            if(vm.inOnSaving() == false){
                vm.object.cropit('imageSrc',val);
            }
        }
        this.export = function(value){
            vm.setOnSaving = true;
            return vm.object.cropit('export',value);
        }
        this.inOnSaving = function(){
            return vm.setOnSaving;
        }
        this.getStyle = function(){
            return vm.object.find('.cropit-preview-image').attr('style');
        }
        this.setStyle = function(newStyle){
            return vm.object.find('.cropit-preview-image').attr('style',newStyle);
        }
        this.disable = function(){
            vm.setOnSaving = false;
            return vm.object.cropit('disable');
        }
        this.enable = function(){
            this.setOnSaving = false;
            return vm.object.cropit('reenable');
        }
        this.fetchImageFromUrl = function(url){
            return vm.object.cropit('imageSrc',url); 
        }
        let vm = this;
        return vm;
    };
    return _cropit;
})