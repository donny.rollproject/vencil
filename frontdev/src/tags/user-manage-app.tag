<user-upload-app ref="root">
	<div  class="profile-box-col-1">
		<div id="pic-box">
			<img ref="the_profile" src={imgSource}>
		</div>
		<div>
			<input id="image_upload" type="file" style="display: none" name="image_upload" class="validate" placeholder="your full name" autocomplete="off">
	        <button class="button profile-box-upload-btn" onclick="{handleUpload.bind(this)}">Upload</button>
	    </div>
	</div>
	<script>
		this.opts.init(this);
	</script>
</user-upload-app>