define(function(){
	let formnya = null;
	const Validator = require('validatorjs');
	let {req_postData} = require('../helpers/publicFunctions.js');
	let init = {};
	init.registerView = function(tagName){
		riot.tag(tagName,false,init.memberRegisterController);
		return riot.mount(tagName);
	}
	init.memberRegisterController = function(opts){
		let vm = this;
		vm.register = {};
		vm.handleRegister = async function(e){
			e.preventUpdate = true;
			e.preventDefault();
			vm.register.full_name = formnya.find('[name=full_name]').val();
			vm.register.username = formnya.find('[name=username]').val();
			vm.register.email = formnya.find('[name=email]').val();
			vm.register.password = formnya.find('[name=password]').val();
			let data = await req_postData({
				action : 'newRegisterMember',
				full_name : vm.register.full_name,
				username : vm.register.username,
				email : vm.register.email,
				password : vm.register.password
			},function(error){
				console.log('error => ',error);
			})
			switch(data.status){
				case 'success':
					window.location.href = data.foward;
					break;
				case 'error':
				case 'rejected':
					break;
			}
		}
		vm.on('mount',function(){
			formnya = $('[data-is='+opts.dataIs+']');
		})
	}
	init.loginView = function(tagName){
		riot.tag(tagName,false,init.memberLoginController);
		return riot.mount(tagName);
	}
	init.memberLoginController = function(opts){
		let vm = this;
		vm.login = {};
		vm.handleLogin = async function(e){
			e.preventUpdate = true;
			e.preventDefault();
			vm.login.username = formnya.find('[name=username]').val();
			vm.login.password = formnya.find('[name=password]').val();
			let data = await req_postData({
				action : 'signinMemberProcess',
				username : vm.login.username,
				password : vm.login.password
			},function(error){
				console.log('error => ',error);
			})
			switch(data.status){
				case 'success':
					window.location.href=data.foward;
				break;
				case 'error':
				case 'rejected':
				break;
			}
		}
		vm.on('mount',function(){
			formnya = $('[data-is='+opts.dataIs+']')
			
		})
	}
	return init;
})