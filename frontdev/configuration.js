let jj = function(){
	this.mode_option = {
		debug : true
	}
	this.local = {
		DEBUG_URL : "https://localhost/the_jobs/html/laravel/static/dist/",
		DIST_URL : "https://localhost/the_jobs/html/laravel/static/dist/"
	}
	this.cloud9 = {
		DEBUG_URL : "https://vencil-rolldone.c9users.io/static/dist/",
		DIST_URL : "https://vencil-rolldone.c9users.io/static/dist/"
	}
	this.cloud9v2 = {
		DEBUG_URL : "http://vencil-rolldone.c9users.io:8082/static/dist/",
		DIST_URL : "http://vencil-rolldone.c9users.io:8082/static/dist/"
	}
	this.local_lan = {
		DEBUG_URL : "http://192.168.0.100/the_jobs/html/laravel/static/dist/",
		DIST_URL : "http://192.168.0.100/the_jobs/html/laravel/static/dist/"
	}
	this.dinamic = {
		DEBUG_URL : 'http://localhost:3000/static/dist/',
		DIST_URL : 'http://localhost:3000/static/dist/'
	}
	this.path_source = this.cloud9v2;
	let vm = this;
	return vm;
}
module.exports = jj;