const localStaticVariable = {
	url_root:'http://localhost:8081',
	url_restapi:'http://localhost:8081',
	url_storage:'http://localhost:8081/images'
}

const lanStaticVariable = {
    url_root:'http://localhost/the_jobs/html/laravel/public',
	url_restapi:'http://localhost/the_jobs/html/laravel/public',
	url_storage:'http://localhost/the_jobs/html/laravel/public/images'
}

const onlineStaticVariable = {
	url_root:'https://sociate.co',
	url_restapi:'https://sociate.co',
	url_storage:'https://sociate.co/images'
}

const cloud9Variable = {
	url_root:'http://vencil-rolldone.c9users.io',
	url_restapi:'http://vencil-rolldone.c9users.io',
	url_storage:'http://vencil-rolldone.c9users.io'
}

export const staticVariable = localStaticVariable;