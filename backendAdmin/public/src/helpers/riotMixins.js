define(function (require) {
    var riot = require('riot');
    var configService = require('./configService.js');
    var temp_callback = null;
    var appConfig = require('../../configuration.js');
    appConfig = new (appConfig)();
    var vm = {
        apiKey : {
            googlePlace : '',
            facebookApi : '',
        },
        // predefine code
        baseURL : configService.staticVariable.url_root,
        // mengubah data uri ke blob
        // cocok untuk di pake dropzone
        mixinDataURItoBlob : function(dataURI) {
            var byteString, 
                mimestring 
            console.log(dataURI);
            if(dataURI.split(',')[0].indexOf('base64') !== -1 ) {
                byteString = atob(dataURI.split(',')[1])
            } else {
                byteString = decodeURI(dataURI.split(',')[1])
            }

            mimestring = dataURI.split(',')[0].split(':')[1].split(';')[0]

            var content = new Array();
            for (var i = 0; i < byteString.length; i++) {
                content[i] = byteString.charCodeAt(i)
            }

            return new Blob([new Uint8Array(content)], {type: mimestring});
        },
        randomTimeLoad : function(){
            return Math.floor(Math.random() * 2000) + 100 
        },
        toMoneyCurrency: function (angka,mataUang) {
            var rev = parseInt(angka, 10).toString().split('').reverse().join('');
            var rev2 = '';
            for (var i = 0; i < rev.length; i++) {
                rev2 += rev[i];
                if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
                    rev2 += '.';
                }
            }
            if(mataUang == ''){
                mataUang = '';
            }else{
                mataUang = mataUang+'. ';
            }
            return mataUang+rev2.split('').reverse().join('') + ',00';
        },
        usernameValid: function(text){
            // bloking comma, space
            var regex = /^[a-zA-Z0-9.\-_$@*!]{3,30}$/;
            if(regex.test(text)){
                return true;
            }
            return false;
        },
        emailValid: function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        passwordValid: function (whatType, whatPassword) {
            switch (whatType) {
                case 'easy-level':
                    if(whatPassword.length < 6){
                        return false;
                    }
                    return true;
                case 'medium-level':
                break;
                case 'hard-level':
                break;
            }
        },
        setState2: function(value,callback){
            let oo = this;
            return new Promise(function (resolve, reject) {
                oo.on('update', function () {
                    if (callback != null) {
                        setTimeout(function(){
                            if(callback != null){
                                callback();
                                callback = null;
                                vm.console.log('State =>','Change State2 is Done!');
                                resolve();
                            }else{
                                vm.console.log('State =>','Call back is null!');
                                resolve();
                            }
                        },0);
                    }
                })
                oo.update(value);
            });
            // override function biar seperti react state
            // untukk page tracking callback punya siapa
            // vm.console.log('Track Callback =>', callback);
            
        },
        console:{
            log:function(title,message){
                if(appConfig.mode_option.debug){
                    console.log(title,message);
                }
            },
            error:function(title,message){
                if(appConfig.mode_option.debug){
                    console.error(title,message);
                }
            }
        },
        LOADER : "<div class='loading'><svg viewBox='0 0 44 44' xmlns='http://www.w3.org/2000/svg' stroke='#129fdd'><g fill='none' fill-rule='evenodd' stroke-width='2'><circle cx='22' cy='22' r='1'><animate attributeName='r' begin='0s' dur='1.8s' values='1; 20' calcMode='spline' keyTimes='0; 1' keySplines='0.165, 0.84, 0.44, 1' repeatCount='indefinite' /><animate attributeName='stroke-opacity' begin='0s' dur='1.8s' values='1; 0' calcMode='spline' keyTimes='0; 1' keySplines='0.3, 0.61, 0.355, 1' repeatCount='indefinite' /></circle><circle cx='22' cy='22' r='1'><animate attributeName='r' begin='-0.9s' dur='1.8s' values='1; 20' calcMode='spline' keyTimes='0; 1' keySplines='0.165, 0.84, 0.44, 1' repeatCount='indefinite' /><animate attributeName='stroke-opacity' begin='-0.9s' dur='1.8s' values='1; 0' calcMode='spline' keyTimes='0; 1' keySplines='0.3, 0.61, 0.355, 1' repeatCount='indefinite' /></circle></g></svg></div>",
        regexUsername : new RegExp("^[a-zA-Z0-9\.\_]+$"),
        firstToUpperCase: function (whatText) {
            var whatIndex = 0;
            var gg = whatText[0].toUpperCase();
            return whatText.substr(0, whatIndex) + gg + whatText.substr(whatIndex + 1);
        },
        observable: riot.observable(),
        init: function(){
            this.on('mount',function(){
                if(this.root.tagName != 'DIV'){
                    $(this.root.tagName).show();
                }else{
                    $('[data-is='+this.opts.dataIs+']').show();
                }
            })
        }
    }
    return vm;
})