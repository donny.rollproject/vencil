$( document ).ready(function() {
	$('.dropdown').each(function () {
        $(this).dropDown();
    });
	
	$('.ruleditable').each(function() {
	    $(this).ruleditable();
	});

});

$.fn.extend({
	dropDown: function () {
        'use strict';
        var self = $(this),
            button = self.children().eq(0),
            dropcontent = self.children().eq(-1),
            span = $("<span class='arrow'></span>"),
            rightpos = parseInt(dropcontent.css('right'), 10);
        
        span.css({right: self.width() / 2 + Math.abs(rightpos)});
        dropcontent.css({top: self.height() + 5});
        dropcontent.append(span);
        
        if (self.hasClass('onhover')) {
            button.on("mouseover", function () {
                self.addClass("active");
            });
            
            button.on("mouseout", function () {
                self.removeClass("active");
            });
            return;
        }
 
        button.on("click", function (e) {
            e.preventDefault();
            self.toggleClass("active");
        });
        
        $(document).on('click', function (event) {
            if (!$(event.target).closest(self).length) {
                self.removeClass("active");
            }
        });
        
	},
	ruleditable: function (options) {
        'use strict';
        var self = $(this),
            settings,
            placeholder;
        
        settings = $.extend({
            edit: true,
            placeholder: 'placeholder..'
        }, options);
        
        placeholder =  $(this).data('placeholder') === '' ? settings.placeholder : $(this).data('placeholder');
        
        self.attr('contenteditable', settings.edit);
        
        if (self.text() === "") {
			self.text(placeholder);
			self.addClass('placeholder');
		}
	
		self.on('focus', function () {
			if (self.text() === placeholder) {
				self.text("");
                self.removeClass('placeholder');
            }
		});
	
		self.on('blur', function () {
			if (self.text() === "") {
                self.text(placeholder);
                self.addClass('placeholder');
            }
		});


		if (settings.edit === false && self.text() === placeholder) {
			self.text('');
        }
       
    }
});