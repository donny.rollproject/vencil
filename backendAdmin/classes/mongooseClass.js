const mongoose 		= require('mongoose')
mongoose.Promise 	= require('bluebird');
/* 
 * Mongoose by default sets the auto_reconnect option to true.
 * We recommend setting socket options at both the server and replica set level.
 * We recommend a 30 second connection timeout because it allows for 
 * plenty of time in most operating environments.
 */
var uriString = 'mongodb://donny:43lw9rj2@ds161574.mlab.com:61574/vencil';
var options = { 
	useMongoClient: true 
}; 
mongoose.connect(uriString,options);
module.exports = {
	mongoose : mongoose,
	uriString : uriString
}