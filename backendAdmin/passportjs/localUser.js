var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy;

// ** serialize passportjs
passport.serializeUser(function(user,done){
	console.log('serializeUser => ',user);
	done(null,user.id);
})

passport.deserializeUser(function(id,done){
	console.log('deserializeUser => ',id);
	done(null,id);
})
var the_local = {
	signIn : function(callback){
		passport.use(new LocalStrategy(
			function(username,password,done){
				callback(username,password,done);
			}
		));
	},
	passport: passport
}

module.exports = the_local;